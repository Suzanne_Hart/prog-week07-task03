﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week07_task03
{
    class Program
    {
        static void Main(string[] args)
        {
            addition(2);
            subtraction(2);
            division(2);
            multiplication(2);

            Console.ReadLine();

        }
        static void addition (int number)
        {
            Console.WriteLine($"{number} + {number}");
        }
        static void subtraction (int number)
        {
            Console.WriteLine($"{number} - {number}");
        }
        static void division (int number)
        {
            Console.WriteLine($"{number} / {number}");
        }
        static void multiplication (int number)
        {
            Console.WriteLine($"{number} * {number}");
        }
    }
}
